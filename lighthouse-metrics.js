const fs = require('fs');
const path = require('path');
const lighthouse = require('lighthouse');
const constants = require('lighthouse/lighthouse-core/config/constants.js');
const chromeLauncher = require('chrome-launcher');

let urlsToCheck = require('./listOfUrls.json');

class WasMetrics {

	async launchChromeAndRunLighthouse() {
		const chrome = await chromeLauncher.launch({ chromeFlags: ['--headless'] });
		let runs = 0;
		for (const elem of urlsToCheck) {
			// delete log-file
			fs.rmSync(`${elem.path}/reportlog.json`, {
				force: true,
			});
			for (const type of elem.type) {
				console.log(`Running performance test ${runs + 1} --> ${type}`); // Logs this to the console just before it kicks off
				try {
					let now = new Date();
					let currDate = `${now.getFullYear()}-${now.getMonth()}-${now.getDate()}`;
					let currTime = `${now.getHours()}-${now.getMinutes()}-${now.getSeconds()}`;

					const options = { logLevel: 'silent', output: 'html', onlyCategories: elem.categories , port: chrome.port };
					const runnerResult = await lighthouse(elem.url, options, this.getConfig(type));
					// `.report` is the HTML report as a string
					const reportHtml = runnerResult.report;
	
					let dir = `${elem.path}/${type}`;
					if (!fs.existsSync(dir)) {
						fs.mkdirSync(dir, { recursive: true });
						console.log('folder created');
					}

					fs.writeFileSync(`./${dir}/report_${currDate}_${currTime}.html`, reportHtml);
					fs.writeFileSync(`./${dir}/report_${currDate}_${currTime}.json`, JSON.stringify(runnerResult.lhr));
					
					// `.lhr` is the Lighthouse Result as a JS object
					console.log('Report is done for', runnerResult.lhr.finalUrl);
					console.log('Performance score was', runnerResult.lhr.categories.performance.score * 100);
				}
				catch (err) {
					console.log(`Performance test ${runs + 1} failed --> ${err}`); // If Lighthouse happens to fail it'll log this to the console and log the error message
					break;
				}
				runs++;
			}
		}

		await chrome.kill();
		return true;
	};

	getConfig(type) {
		let config = {};
		if (type.toLowerCase() === 'desktop') {
			config = {
				extends: 'lighthouse:default',
				settings: {
					formFactor: 'desktop',
					throttling: constants.throttling.desktopDense4G,
					screenEmulation: constants.screenEmulationMetrics.desktop,
					emulatedUserAgent: constants.userAgents.desktop
				}
			}
		} else if (type.toLowerCase() === 'mobile') {
			config = {
				extends: 'lighthouse:default'
			}
		}
		return config;
	}

	generateReportLogs() {
		for (const elem of urlsToCheck) {
			let files = [];
			fs.writeFileSync(`${elem.path}/reportlog.json`, JSON.stringify(this.getFilesRecursively(elem.path, elem.path, files), null, 2));
		}
		console.log('logs generated successfully');
	};

	getFilesRecursively(directory, prefix, files) {
		const filesInDirectory = fs.readdirSync(directory);
		for (const file of filesInDirectory) {
			const absolute = path.join(directory, file);
			if (fs.statSync(absolute).isDirectory()) {
				this.getFilesRecursively(absolute, prefix, files);
			} else {
				if (path.extname(absolute) === '.json') {
					prefix = prefix.replace('/', '\\');
					files.push(absolute.replace(prefix, ''));
				}
			}
		}
		return files;
	}
}

const Lh = new WasMetrics;
const light = Lh.launchChromeAndRunLighthouse();
light.finally(resp => {
	Lh.generateReportLogs();
})