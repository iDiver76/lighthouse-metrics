import { default as Highcharts } from "./Highcharts-8.1.0/code/es-modules/masters/highcharts.src.js";
class OpaccMetrics {
	constructor() {
		this.chartElements = document.querySelectorAll('.metrics__chart');
		this.dashboardElement = document.querySelector('#js-metrics-container');
		this.metricsTplElement = document.querySelector('#js-metrics-item');
		this.formFactorObj = {
			desktop: {
				label: 'Desktop',
			},
			mobile: {
				label: 'Mobile',
			}
		}

		if (!this.dashboardElement && !this.metricsTplElement) return;

		this.attrUuid =  'id';
		this.appInit();
	}

	async appInit() {
		this.dataSource = await this.getDataSource();
		this.initMetrics();
	}

	async getDataSource(source = './../../listOfUrls.json', transformResponse = true) {
		if (transformResponse) {
			const response = await fetch(source);
			return await response.json();
		}
		return await fetch(source);
	}

	async initMetrics() {
		for (const item of this.dataSource) {
			let reports = await this.getDataSource(`${item.path}/reportlog.json`);

			let metricsTpl = document.importNode(this.metricsTplElement.content, true);
			let chartElem = metricsTpl.querySelector('.metrics__chart');
			if (!chartElem) return;
			let uuid = this.uuidv4();
			this.setUuid(chartElem, uuid, this.attrUuid);

			let datasets = [];

			for (const [idx, report] of reports.entries()) {
				if (report.indexOf('reportlog.json') > 0) {
					return;
				}
				let path = `${report.replaceAll('\\', '/')}`;

				// load json-file
				let lhData = await this.getDataSource(path, false);
				// ref to Lighthouse-Report
				let htmlReport = null;
				htmlReport = lhData.url.replace('.json', '.html');

				let jsonResp = await lhData.json();
				// prepare Series for Highcharts
				datasets.push(this.setSeries(jsonResp, htmlReport));
				if (idx === reports.length-1) {
					let reducedArray = this.reduceDataSetArrayByType(datasets, 'name');
					let chartData = this.setChartData(reducedArray, item.title);
					this.dashboardElement.appendChild(metricsTpl);
					new Highcharts.chart(uuid, chartData);
				}
			}
		}
	}

	reduceDataSetArrayByType(datasets, property) {
		let groupedDatasets = datasets.reduce((acc, obj) => {
			let key = obj[property];
			let tmpObj = Object.assign({},obj);
			if(!acc[key]) {
				acc[key] = [];
				tmpObj.data = [obj.data];
				acc[key].push(tmpObj);
			} else {
				acc[key][0].data.push(obj.data);
			}
			return acc;
		}, {});

		let retVal = [];
		for (const [key, value] of Object.entries(groupedDatasets)) {
			retVal.push(...value);
		}
		return retVal;
	}

	setChartData(datasets, chartTitle) {
		console.log('datasets', datasets);
		return {
			chart: {
				type: 'spline',
				zoomType: 'x'
			},
			time: {
				timezone: 'Europe/Zurich',
				useUTC: false
			},
			title: {
				text: chartTitle,
			},
			subtitle: {
				text: 'Lighthouse Performance score'
			},
			xAxis: {
				type: 'datetime',
				// tickInterval: 6 * 3600 * 1000,
				// labels: {
				// 	formatter: function() {
				// 	  return Highcharts.dateFormat('%d %b %Y', this.value);
				// 	}
				//   },
				title: {
					text: 'Date'
				}
			},
			yAxis: {
				title: {
					text: 'Score'
				},
				max: 100
			},
			series: datasets,
			tooltip: {
				headerFormat: '<h3>{series.name}</h3>',
				pointFormat: '<p><a href="{point.htmlReport}" target="_blank">See Lighthouse report</a></p><p>{point.x:%e. %b %Y - %H:%M}<br/><b>{point.y}</b> / 100</p>',
				style: {
					pointerEvents: 'auto'
				},
				stickOnContact: true,
				useHTML: true,
				hideDelay: 1000
			},
			credits: {
				enabled: false
			},
			plotOptions: {
				series: {
					stickyTracking: false,
				  	marker: {
						enabled: true
				  	}
				}
			  },
			responsive: {
				rules: [{
					condition: {
						maxWidth: 500
					},
					chartOptions: {
						legend: {
							layout: 'horizontal',
							align: 'center',
							verticalAlign: 'top'
						}
					}
				}]
			}
		}
	}

	setSeries(elem, filename) {
		return {
			name: (elem.configSettings.formFactor.toLowerCase() === 'desktop') ? this.formFactorObj.desktop.label : this.formFactorObj.mobile.label, //'Desktop or Mobile'
			data: {x: Date.parse(elem.fetchTime), y:(elem.categories.performance.score * 100), htmlReport: filename}
		}
	}

	/**
	 * Generate UUID
	 *
	 * @returns {string}  UUID
	 */
	uuidv4() {
		return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c => (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16) );
	}

	/**
	 * set uuid on element
	 *
	 * @param {HTMLElement} elem Element
	 * @param {string} id Uuid
	 * @param {string} datasetAttr Dataset Attribut
	 */
	setUuid(elem, uuid, dataAttr ){
		elem.setAttribute(dataAttr, uuid);
	}

	/**
	 * get uuid of the element
	 *
	 * @param {HTMLElement} elem Element
	 * @param {string} datasetAttr Dataset Attribut
	 */
	getUuid(elem, dataAttr){
		return elem.getAttribute(dataAttr);
	}
}

new OpaccMetrics();