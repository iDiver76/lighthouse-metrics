# LIGHTHOUSE Metrics

## Systemanforderungen
- volta.sh installiert
## Install
- npm install

## Starten

```
npm run metrics
```

### Welche Url's werden getestet?
In der Datei "listOfUrls.json" werden als Objekt alle zu testenden Url's hinterlegt.

```
{
	"url": "https://fo-wastest-performance.was.local/de/startnocat.htm",
	"path": "perfkit/start-nocat",
	"title": "Startseite ohne Katalog",
	"type": ["desktop", "mobile"],
	"categories": ["performance"]
}
```

Folgende Optionen können als Key/Value - Pair definiert werden:
| Key | Type | Value |
| - | - | - |
| url | string | url, welche geprüft werden soll |
| path | string | relativer Pfad, an welchem die Reports gespeichert werden. |
| title | string | Titel, welcher in der Grafik angezeigt werden soll. |
| type | array | Emulator-Typ<br/><ul><li>desktop</li><li>mobile</li></ul> |
| categories | array | Lighthouse TestKategorie<br/><ul><li>performance</li><li>pwa</li><li>seo</li><li>accessibility</li><li>best-practices</li></ul> |

## UI (Dashboard)
Im Dashboard werden die ermittelten Daten als Grafiken angezeigt.

> Für die Erstellung der Grafiken wird "highcharts.js" als JavaScript-Library verwendet.
### Alpha-Status des UI
Das Dashboard befindet sich noch in der Entwicklung. Aktuell kann das Dashboard nur lokal auf der Entwicklungsmaschine mittels lokalem Webserver gestartet werden.

> Um das Dashboard via VSCode zu starten, wird das VSCode Plugin "[Live Server](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer)" empfohlen.